import 'package:flutter/material.dart';

class ProducDetailsView extends StatelessWidget {

  final String _name;
  final double _price;
  final String _image;
  final String _description;


  const ProducDetailsView({super.key, required String name, required String image, required String description, required double price}) : _name = name, _price=price, _description=description, _image=image;
  

  @override
  Widget build(BuildContext context) => Scaffold
  (

    appBar: AppBar
    (
      title: const Text('Detalles del producto'),
      backgroundColor: Colors.deepPurple,
    ),

    body: Column
      (
        children: 
        [
          const Spacer(flex: 1),

          Container
          (
            
            width: 250,
            height: 250,
            decoration: BoxDecoration
            (
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: 
              [
                BoxShadow
                (
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 10,
                  blurRadius: 10,
                ),
              ],
            ),
            child:   Hero
            (
              tag: _name,
              child: Image.asset( _image),
            ),
  
          ) ,
            
          

          const Spacer(flex: 1),

          Container
          (
            
            width: 500,
            height: 400,
            decoration: BoxDecoration
            (
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: 
              [
                BoxShadow
                (
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 10,
                  blurRadius: 10,
                ),
              ],
            ),
            child: Column
            (
              
              children: 
              [
                const Spacer(flex: 1),
                
                Text(_name,  textAlign: TextAlign.left,),
                
                const Spacer(flex: 1),
                
                Text("€" + _price.toString(),  textAlign: TextAlign.left,),

                const Spacer(flex: 1),

                Text(_description,  textAlign: TextAlign.left,),

                const Spacer(flex: 10),
                
                
              ],
            ),
          ),



        ]
      )
   

  );
}