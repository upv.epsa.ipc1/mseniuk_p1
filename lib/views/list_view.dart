import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:mseniuk_p1/models/product_model.dart';
import 'package:mseniuk_p1/views/product_details_view.dart';
import 'dart:developer' as dev;

class ProductsListView extends StatelessWidget 
{
  const ProductsListView({super.key,});

  Future<String> loadFile(BuildContext context) async 
  {
    var result = await DefaultAssetBundle.of(context).loadString('assets/json/products.json');
    dev.log(result);
    return result;
  }
  
  @override
  Widget build(BuildContext context) => Scaffold
  (

    appBar: AppBar
    (
      title: const Text('Productos'),
      backgroundColor: Colors.deepPurple,
    ),

    body: Center
      (
        child: FutureBuilder
        (
          future:  loadFile(context), 
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) 
          {  
            if(snapshot.hasData)
            {
              var products = Products.fromJson(jsonDecode(snapshot.data!));
              var product = products.products.map((aux) => ListTile
              (
                leading: Hero
                (
                  tag: aux.productName,
                  child: Image.asset
                  ( 
                    aux.imagePath,
                    width: 50,
                    height: 50, 
                  ),
                ),
                
                

                title: Text(aux.productName),
                subtitle: Text( aux.productPrice.toString() ),
                trailing: const Icon(Icons.arrow_forward_ios_rounded),
                onTap: () 
                {
                  var route = MaterialPageRoute
                  (
                    builder: (context) => ProducDetailsView(name: aux.productName,image: aux.imagePath,description: aux.productDescription,price: aux.productPrice,),
                  );
                  Navigator.of(context).push(route);
                }
              ))
              .toList();

              return ListView.builder
              (
                padding: const EdgeInsets.all(2),
                itemBuilder: (context, index) => product[index],
                itemCount: product.length,
              );
            }
            return const Center(child: CircularProgressIndicator());
          },
        )
      )
  );
}