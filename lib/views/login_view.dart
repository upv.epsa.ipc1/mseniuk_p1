import 'package:flutter/material.dart';
import 'package:mseniuk_p1/views/list_view.dart';

class LoginView extends StatelessWidget 
{
  const LoginView({super.key,});
  @override
  
  Widget build(BuildContext context) 
  {
    return Scaffold(
      body: Column
      (
        children: 
        [
          const Spacer(flex: 2),
          
          Row
          (
            children: 
            [
              const Spacer(flex: 2),
              
              const SizedBox
              (
                width: 100,
                height: 50,
                child:  FittedBox( child: Text('Login'),),
              ),
                
              const Spacer(flex: 20),
                
              Container
              (
                width: 40, 
                height: 40, 
                decoration: const BoxDecoration
                (
                  color: Colors.deepPurple,
                  shape: BoxShape.circle,
                ),
              ),
                
              const Spacer(flex: 1),
                
              Container
              (
                width: 80, 
                height: 80, 
                decoration: const BoxDecoration
                (
                  color: Colors.deepPurple,
                  shape: BoxShape.circle,
                ),
              ),
              Container
              (
                width: 20, 
                height: 40, 
                decoration: const BoxDecoration
                (
                  color: Colors.deepPurple,
                  shape: BoxShape.circle,
                ),
              ),
              
              const Spacer(flex: 2),
            ],
          ),
          
          const Spacer(flex: 5),

          Container
          (
            width: 300,
            height: 200,
            decoration: BoxDecoration
            (
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: 
              [
                BoxShadow
                (
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 10,
                  blurRadius: 10,
                ),
              ],
            ),
            child: const Column
            (
              children: 
              [
                
                Spacer(flex: 5),
                
                TextField
                (
                  decoration: InputDecoration
                  (
                    prefixIcon: Icon(Icons.person, color: Colors.deepPurple),
                    border: UnderlineInputBorder(),
                    labelText: 'Username',
                    hintText: 'Enter name'
                  ),
                ),
                
                Spacer(flex: 1),
                
                TextField
                (
                  decoration: InputDecoration
                  (
                    prefixIcon: Icon(Icons.key, color: Colors.deepPurple),
                    border: UnderlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter your password'
                  ),
                ),
                
                Spacer(flex: 3),
              ],
            ),
          ),
          
          const Spacer(flex: 5),
          
          Align
          (
            alignment: Alignment.centerRight,
            child: GestureDetector
            (
              onTap: () => _goToSecondPage(context),
              child: Container
              (
                width: 150,
                height: 40, 
                decoration: const BoxDecoration
                (
                  color: Colors.deepPurple,
                  borderRadius: BorderRadius.only
                  (
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.zero,
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.zero,
                  ),
                ),
                child: const Center ( child: Text('Login',style: TextStyle(color: Colors.white, fontSize: 25)),),
              ),
            ),
          ),
          
          const Spacer(flex: 5),
        ]
      ),
    );
  }
  void _goToSecondPage(BuildContext context) 
  {
    var route = MaterialPageRoute
    (
      builder: (context) => const ProductsListView(),
    );
    Navigator.of(context).push(route);
  }
}
