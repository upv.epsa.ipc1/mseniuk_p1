/*import 'package:flutter/material.dart';
import 'package:mseniuk_p1/views/product_details_view.dart';*/

class ProductModel {

  late String productName;
  late double productPrice;
  late String imagePath;
  late String productDescription;

  ProductModel
  ({
    required this.productName, 
    required this.productPrice, 
    required this.imagePath,
    required this.productDescription, 
  });

  ProductModel.fromJson(Map<String, dynamic> json) 
  {
    productName = json['name'];
    productPrice = json['price'];
    imagePath = json['image'];
    productDescription = json['description'];
  }

  /*String getProductName () 
  {
    return productName;
  }

  double getProductPrice () 
  {
    return productPrice;
  }

  String getImagePath () 
  {
    return imagePath;
  }

  String getProductDescription () 
  {
    return productDescription;
  }

  Widget toListTile(BuildContext context) => ListTile
  (
    leading: Image.asset(imagePath),
    title: Text(productName),
    subtitle: Text(productPrice.toString()),
    trailing: const Icon(Icons.arrow_forward_ios_rounded),
    onTap: () => _goToNext(context),
  );

  void _goToNext(BuildContext context) 
  {
    var route = MaterialPageRoute
    (
      builder: (context) => ProducDetailsView(),
    );
    Navigator.of(context).push(route);
  }*/
}

class Products 
{
  late List<ProductModel> products;

  Products({required this.products});

  Products.fromJson(Map<String, dynamic> json) 
  {
    if (json['products'] != null) 
    {
      products = <ProductModel>[];
      json['products'].forEach((v) 
      {
        products.add(ProductModel.fromJson(v));
      });
    }
  }
}